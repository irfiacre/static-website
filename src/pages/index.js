import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import Seo from "../components/seo"
import * as styles from "../components/index.module.css"


const IndexPage = () => (
  <Layout>
      <h1>
        Welcome to <b>Top Spot Dev</b>
      </h1>
    <div className={styles.textCenter}>

      <StaticImage
        src="https://picsum.photos/1200/800.jpg"
        loading="eager"
        formats={["auto", "webp", "avif"]}
        alt=""
        style={{ marginBottom: `var(--space-3)` }}
      />
      
      <p className={styles.intro}>
        This is a site used for learing GitLab CI/CD basics 
      </p>
      <h1> This is a Test branch </h1>
      <p  className={styles.list}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus in finibus quam. Nulla elementum suscipit elit, at euismod risus interdum vel. Nulla in metus velit. Nunc ut tincidunt sem. Ut vel blandit turpis. Maecenas in ligula dui. Donec sagittis metus sit amet sapien congue dapibus. Donec iaculis purus at suscipit gravida. Nunc sodales commodo dui. Phasellus erat turpis, egestas non blandit in, cursus eu purus.

            Curabitur tincidunt vel massa eget laoreet. Maecenas a pellentesque massa, at ornare risus. Suspendisse condimentum finibus tellus nec feugiat. Pellentesque risus neque, tincidunt sit amet odio id, aliquam maximus justo. Aliquam non scelerisque metus. Suspendisse fermentum metus ut turpis tincidunt viverra. Nam congue orci ac nisl tincidunt tempor.
      </p>
      </div>

      <div>
        <span>Version: <i>%%VERSION%%</i></span>
      </div>
  </Layout>
)

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Home" />

export default IndexPage
